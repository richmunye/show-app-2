import { Layout } from 'antd';
import HeaderBar from '../components/layouts/header';
import LeftSideBar from '../components/layouts/leftSidebar';
import RightSidebar from '../components/layouts/rightSidebar';

const Dashboard = () => {
  return (
    <Layout style={{ display: 'flex' }}>
      <LeftSideBar />
      <Layout>
        <HeaderBar />
        <RightSidebar />
      </Layout>
    </Layout>
  );
};
export default Dashboard;
