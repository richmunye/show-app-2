import { HomeFilled } from '@ant-design/icons';
import { BsFillPersonFill } from 'react-icons/bs';
import { AiFillSetting } from 'react-icons/ai';
import { IoReturnUpBackOutline } from 'react-icons/io5';
export const MenuLinks = [
  {
    id: 1,
    label: 'Home',
    icon: (
      <HomeFilled
        style={{
          margin: 'auto',
          fontSize: '2.4em',
          color: '#c12b3d',
        }}
      />
    ),
  },
  {
    id: 2,
    label: 'Profile',
    icon: (
      <BsFillPersonFill
        style={{ margin: 'auto', fontSize: '2.4em' }}
      />
    ),
  },
  {
    id: 3,
    label: 'Settings',
    icon: (
      <AiFillSetting
        style={{ margin: 'auto', fontSize: '2.4em' }}
      />
    ),
  },
  {
    id: 4,
    label: 'Logout',
    icon: (
      <IoReturnUpBackOutline
        style={{ margin: 'auto', fontSize: '2.4em' }}
      />
    ),
  },
];
