import { Layout, Typography } from 'antd';
const { Text } = Typography;

export const ProgressBar = () => {
  return (
    <Layout
      className="white_background"
      style={{
        width: '100%',
        padding: '20px',
        margin: 'auto',
        lineHeight: '2.2',
      }}
    >
      <Text style={{ paddingBottom: '12px' }}>
        Marketing{' '}
        <Text style={{ color: '#c12b3d' }}>-140</Text>
      </Text>
      <Layout
        className="grey_background"
        style={{
          width: '90%',
          height: 'auto',
          backgroundColor: '#c4c4c4',
        }}
      >
        <Layout
          className="red_background"
          style={{
            width: '70%',
            height: 8,
            backgroundColor: '#c12b3d',
          }}
        ></Layout>
      </Layout>
    </Layout>
  );
};
