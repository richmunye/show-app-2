import {
  Layout,
  Space,
  Typography,
  Row,
  Col,
  Button,
} from 'antd';
import { Card } from '../../services/statsCard';
import { ProgressBar } from '../../services/progressbars';
import './rightSidebar.css';
const { Sider } = Layout;
const { Text, Title } = Typography;

const RightSidebar = () => {
  return (
    <Sider width={450} className="right-sidebar-container">
      <Title level={3}>Stats</Title>
      <Row className="cards-holder" justify="space-between">
        {Card.map((card) => (
          <Col span={2} className="card">
            <Layout>
              <Space style={{ padding: '12px' }}>
                <Text
                  style={{
                    color: '#af4304',
                  }}
                >
                  {card.stats}
                </Text>
                <Text style={{ marginTop: '22px' }}>
                  {card.name}
                </Text>
              </Space>
            </Layout>
          </Col>
        ))}
      </Row>
      <Title className="interest-holder" level={2}>
        Followers interests
      </Title>
      <Layout className="progressbar">
        <Space direction="vertical">
          <ProgressBar className="progress" />
          <ProgressBar />
          <ProgressBar />
          <ProgressBar />
        </Space>
      </Layout>
      <Button className="update-btn">
        UPDATE TO PREMIUM
      </Button>
    </Sider>
  );
};
export default RightSidebar;
