import { Layout, Space, Typography } from 'antd';
import Logo from '../../assets/red_icon.png';
import { MenuLinks } from '../../services/leftSideServices';
import './leftSidebar.css';

const { Sider } = Layout;
const { Text } = Typography;

const LeftSideBar = () => {
  return (
    <Sider width={100} className="left-sidebar-container">
      <Layout className="logo-container">
        <img
          className="menu-link"
          style={{ width: '40px' }}
          src={Logo}
          alt="Home"
        />
      </Layout>
      <Space
        style={{ width: '8vw' }}
        className="link-container"
        direction="vertical"
        size={50}
      >
        {MenuLinks.map((link) => (
          <Layout className="menu-link">
            {link.icon}
            <Text className="menu-text">{link.label}</Text>
          </Layout>
        ))}
      </Space>
    </Sider>
  );
};
export default LeftSideBar;
