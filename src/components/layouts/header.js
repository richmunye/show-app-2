import {
  Layout,
  Space,
  Typography,
  Row,
  Col,
  Avatar,
} from 'antd';
import './headerbar.css';
import { AiOutlineQuestionCircle } from 'react-icons/ai';
import { FaUserCircle } from 'react-icons/fa';
import { IoIosNotifications } from 'react-icons/io';

const { Header } = Layout;
const { Text } = Typography;

const HeaderBar = () => {
  return (
    <Header className="header-container">
      <Row className="header-row">
        <Col
          span={12}
          style={{ margin: 'auto', marginLeft: '23px' }}
        >
          <Text className="header-title">
            Organizer Portal
          </Text>
        </Col>
        <Col className="profile-content" span={12}>
          <Space
            style={{
              width: '100%',
              display: 'flex',
              justifyContent: 'space-between',
              margin: 'auto',
              marginRight: '23px',
            }}
            size={60}
            direction="horizontal"
          >
            <AiOutlineQuestionCircle
              style={{
                fontSize: '1.4em',
                color: '#c4c4c4',
              }}
            />
            <Space className="profile">
              <Avatar
                size={30}
                icon={
                  <FaUserCircle
                    style={{
                      fontSize: '1.4em',
                      color: '#c4c4c4',
                    }}
                  />
                }
              />
              <Text> Yves Honore</Text>
            </Space>
            <IoIosNotifications
              style={{
                fontSize: '1.4em',
                color: '#c4c4c4',
              }}
              className="notifications"
            />
          </Space>
        </Col>
      </Row>
    </Header>
  );
};
export default HeaderBar;
