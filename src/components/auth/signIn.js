import React from 'react';
import { useHistory } from 'react-router-dom';
import {
  Layout,
  Form,
  Input,
  Space,
  Typography,
  Button,
} from 'antd';
import { Link } from 'react-router-dom';
import './signIn.css';
import Logo from '../../assets/red_icon.png';
const { Text, Title } = Typography;
const SignIn = () => {
  let history = useHistory();
  const redirectToDashboard = () => {
    history.push('/dashboard');
  };
  return (
    <Layout className="signin-container">
      <Layout className="clip-container"></Layout>
      <Layout className="content">
        <Layout className="loader"></Layout>
        <Layout className="container">
          <img src={Logo} alt="logo" />
          <Layout className="form-content">
            <Title level={2}>Organizer login</Title>
            <Text className="login-text">
              use your company email to login
            </Text>
            <Form
              className="form-data"
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
            >
              <Form.Item
                className="form-item"
                label="Email"
                name="Email"
                rules={[
                  {
                    required: false,
                    message: 'Please input your username!',
                  },
                ]}
              >
                <Input placeholder="Email" />
              </Form.Item>

              <Form.Item
                className="form-item"
                label="Password"
                name="password"
                rules={[
                  {
                    required: false,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Space
                  style={{ width: '100%' }}
                  direction="horizontal"
                >
                  <Input.Password
                    className="input-password"
                    placeholder="input password"
                  />
                </Space>
              </Form.Item>

              <Form.Item
                className="form-btns"
                wrapperCol={{ offset: 8, span: 16 }}
              >
                <Button
                  className="form-button"
                  onClick={redirectToDashboard}
                  type="primary"
                  htmlType="submit"
                >
                  Login
                </Button>
              </Form.Item>
              <Form.Item className="forgot-password">
                forgot Password?
                <strong>
                  <Link
                    className="form-link"
                    to="/forgot-password"
                  >
                    Reset
                  </Link>
                </strong>
              </Form.Item>
            </Form>
          </Layout>
        </Layout>
      </Layout>
    </Layout>
  );
};
export default SignIn;
