import React from 'react';
import { useHistory } from 'react-router-dom';
import {
  Layout,
  Form,
  Input,
  Typography,
  Button,
} from 'antd';
import { Link } from 'react-router-dom';
import './signIn.css';
import Logo from '../../assets/red_icon.png';
const { Text, Title } = Typography;

const ForgotPassword = () => {
  let history = useHistory();
  const ResetPassword = () => {
    history.push('/reset-password');
  };
  return (
    <Layout className="signin-container">
      <Layout className="clip-container"></Layout>
      <Layout className="content">
        <Layout className="loader"></Layout>
        <Layout className="container">
          <img src={Logo} alt="logo" />
          <Layout className="form-content">
            <Title level={2}>Forgot password</Title>
            <Text className="login-text">
              use your ShowApp email to login to reset your
              password.
            </Text>
            <Form
              className="form-data"
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
            >
              <Form.Item
                className="form-item"
                label="Email"
                name="Email"
                rules={[
                  {
                    required: false,
                    message: 'Please input your username!',
                  },
                ]}
              >
                <Input placeholder="Email" />
              </Form.Item>

              <Form.Item
                className="form-btns"
                wrapperCol={{ offset: 8, span: 16 }}
              >
                <Button
                  type="submit"
                  className="form-button"
                  onClick={ResetPassword}
                >
                  Submit
                </Button>
              </Form.Item>
              <Form.Item className="forgot-password">
                Recall password?
                <strong>
                  <Link className="form-link" to="/">
                    Login
                  </Link>
                </strong>
              </Form.Item>
            </Form>
          </Layout>
        </Layout>
      </Layout>
    </Layout>
  );
};
export default ForgotPassword;
