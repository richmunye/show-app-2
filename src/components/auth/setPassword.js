import React from 'react';
import { useHistory } from 'react-router-dom';
import {
  Layout,
  Form,
  Input,
  Space,
  Typography,
  Button,
} from 'antd';
import { Link } from 'react-router-dom';
import './signIn.css';
import Logo from '../../assets/red_icon.png';
const { Text, Title } = Typography;

const ResetPassword = () => {
  let history = useHistory();
  const redirectToDashboard = () => {
    history.push('/');
  };
  return (
    <Layout className="signin-container">
      <Layout className="clip-container"></Layout>
      <Layout className="content">
        <Layout className="loader"></Layout>
        <Layout className="container">
          <img src={Logo} alt="logo" />
          <Layout className="form-content">
            <Title level={2}>Set new password</Title>
            <Text className="login-text">
              Lorem ipsum dolor sit amet, Consecteur
              adipsicing elit.
            </Text>
            <Form
              className="form-data"
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
            >
              <Form.Item
                className="form-item"
                label="Password"
                name="password"
                rules={[
                  {
                    required: false,
                    message: 'Please input your password!',
                  },
                ]}
              >
                <Space
                  style={{ width: '100%' }}
                  direction="horizontal"
                >
                  <Input.Password
                    className="input-password"
                    placeholder="input password"
                  />
                </Space>
              </Form.Item>

              <Form.Item
                className="form-btns"
                wrapperCol={{ offset: 8, span: 16 }}
              >
                <Button
                  className="form-button"
                  onClick={redirectToDashboard}
                  type="primary"
                  htmlType="submit"
                >
                  Change Password
                </Button>
              </Form.Item>
              <Form.Item className="forgot-password">
                Recall password?
                <strong>
                  <Link className="form-link" to="/">
                    Login
                  </Link>
                </strong>
              </Form.Item>
            </Form>
          </Layout>
        </Layout>
      </Layout>
    </Layout>
  );
};
export default ResetPassword;
