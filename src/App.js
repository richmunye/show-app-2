import React from 'react';
import SignIn from './components/auth/signIn';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import ForgotPassword from './components/auth/forgotPassword';
import ResetPassword from './components/auth/setPassword';
import Dashboard from './views/dashboard';

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={SignIn} />
        <Route
          exact
          path="/forgot-password"
          component={ForgotPassword}
        />
        <Route
          exact
          path="/reset-password"
          component={ResetPassword}
        />
        <Route
          exact
          path="/dashboard"
          component={Dashboard}
        />
      </Switch>
    </Router>
  );
}

export default App;
